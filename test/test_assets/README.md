# Test Assets

## cube.stl

Perfect cube with length of 2 for all sides and a center of gravity at origin.

## 2_4_8_cuboid.stl

Cube with dimensions:

- height = 2
- depth = 4
- width = 8

Center of gravity at origin.

## sphere.stl

Perfect sphere with radius of 1 and a center of gravity at origin.

## slender_rod.stl

Negligible thickness cylinder with length of 10, radius of 0.1, and a center of gravity at origin.

## cylinder.stl

Cylinder with length of 10, radius of 1, and a center of gravity at origin.
