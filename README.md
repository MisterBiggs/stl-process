# stlProcess.jl

Package for getting mass and other properties from `stl` mesh files.

usage: https://gitlab.com/orbital-debris-research/directed-study/report-3/-/blob/main/data.jl
